# DNS - Domain Name System

It's a system that assigns web address/domains to servers IP
    EG:
    name ---> server
    www.bbc.co.uk ---> 43.21.1.22
When you register a domain you control its records

## DNS Records

There are 8 DNS record
- A - Points domain name to IPV4
- AAAA - Like A but its for IPV6    
  
- CNAME - Canonical NAME record - it is used instead of an A record when a domain or sub domanin is an alias of another domain. All CNAME records must point to a domain, never an ip address.  EG: name.com ---> othername.com. Sets several domains to one, like bbc.co.uk ---> bbc.com. Can be used for change of brand
  
- PTR

- NS - NAME SERVER - Which DNS server has authority over domain (priority security)
  
- MX - Mail eXchanger - which server can send emails with this name

- SOA
- TXT

When you buy a domain name you set the records. The records are replicated world wide.

You can check records using DNS checker


## DNS service providers

These providers allow you to register (buy) a domain
- godaddy.com
- gandi.net
- godomain.com
- namecheap.pt

ICAAN - registers dns service providers request
lookup.icaan.org/ can lookup website registry


## Route 53 

In AWS this manages DNS

- Manages DNS +
- Instantances DNS propagation
- DNS Managingaccording to GeoLocation

This can reduce latency and deal with compliance


checking branches